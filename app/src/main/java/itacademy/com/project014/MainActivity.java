package itacademy.com.project014;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvTime;

    private Drawer drawer;

    private AccountHeader header;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvTime = findViewById(R.id.tvTime);

        Button btnStart = findViewById(R.id.btnStart);

        btnStart.setOnClickListener(this);

        initDrawer();
        /*final Handler handler = new Handler();

        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                String time = sdf.format(new Date(System.currentTimeMillis()));
                tvTime.setText(time);
                handler.postDelayed(this, 1000);
            }
        };

        handler.post(runnable);*/

        /*CountDownTimer timer = new CountDownTimer(10000, 1000) {
            @Override
            public void onTick(long millisToFinished) {
                tvTime.setText("Seconds remaining " + millisToFinished / 1000);
            }

            @Override
            public void onFinish() {
                tvTime.setText("Time is over!");
            }
        };

        timer.start();*/

    }

    private void setUserData() {
        // sqlite operation
    }

    @Override
    public void onClick(View view) {
//        header.getActiveProfile().getName().setText("ASDASDASDASD");
//        header.getActiveProfile().getEmail().setText("ASDASDASDASD");
        header.getActiveProfile().withName("asdasdasdasd");
        header.getActiveProfile().withEmail("asdasdasdasd");
    }

    private void initDrawer() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        header = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.header)
                .addProfiles(
                        new ProfileDrawerItem()
                                .withName("Chyngyz")
                                .withEmail("chyngyz.isakov.5@gmail.com")
                                .withIcon(R.mipmap.ic_launcher)
                )
                .withOnAccountHeaderProfileImageListener(listener)
                .build();

        drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(header)
                .build();
    }

    AccountHeader.OnAccountHeaderProfileImageListener listener = new AccountHeader.OnAccountHeaderProfileImageListener() {
        @Override
        public boolean onProfileImageClick(View view, IProfile profile, boolean current) {
            Intent intent = new Intent(MainActivity.this, PersonalCabinet.class);
            startActivity(intent);
            return false;
        }

        @Override
        public boolean onProfileImageLongClick(View view, IProfile profile, boolean current) {
            return false;
        }
    };
}
